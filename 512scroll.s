; 512scroll by Ciaran Anscomb 2021-2023
; For Dragon/Tandy CoCo 1/2 (4K compatible!).

; Tiny demo of manually invoking VDG row preset.

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

DEBUG		set 0		; 1 = wait for shift each pixel scroll

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; PIA0
reg_pia0_ddra	equ $ff00
reg_pia0_pdra	equ $ff00
reg_pia0_cra	equ $ff01
reg_pia0_ddrb	equ $ff02
reg_pia0_pdrb	equ $ff02
reg_pia0_crb	equ $ff03

; PIA1
reg_pia1_ddra	equ $ff20
reg_pia1_pdra	equ $ff20
reg_pia1_cra	equ $ff21
reg_pia1_ddrb	equ $ff22
reg_pia1_pdrb	equ $ff22
reg_pia1_crb	equ $ff23

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	if DISK
		; Disk version resides higher in RAM.
		org $2000
	else
		; Tape version located so that it'll run on a 4K machine.
		org $0800
	endif

start

		orcc #$50

		; DP=$ff for speed of mode changes
		lda #$ff
		tfr a,dp
		setdp $ff

		; Clear screen with background pattern
		ldx #$0400
		ldd #$2020
!		std ,x++
		cmpx #$0600
		blo <
		ldx #$0420
		ldd #$8920
!		std ,x++
		adda #$10
		eora #$0f
		ora #$80
		cmpx #$05e0
		blo <

		; Default would be to trigger FS on hi->lo (or on CoCos
		; without ECB, not enabled at all).
		lda #$37
		sta reg_pia0_crb	; FS enabled lo->hi

		; Waste lots of space with machine detection...
		ldx #0
		lda reg_pia0_pdrb	; clear outstanding IRQ
		sync			; wait for FS
		lda reg_pia0_pdrb	; clear new IRQ
!		mul			; 11
		mul			; +11 = 22
		mul			; +11 = 33
		mul			; +11 = 44
		leax 1,x		; +5 = 49
		lda >reg_pia0_crb	; +5 = 54 - test for IRQ
		bpl <			; +3 = 57 - no?  loop
		ldb #37			; NTSC machine (37)
		cmpx #288		; PAL = 312, NTSC = 262
		blo >			; NTSC?  use default value
		ldb #62			; PAL Dragon (62)
		lda $a000		; [$A000] points to ROM1 in CoCos
		anda #$20
		beq >
		decb			; PAL CoCo (61)
!		stb topdelay

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

restart		ldu #message
mainloop

		; Prerender initial cell contents
		ldx #$042e
		leay ,u
		ldb #14
!		lda ,y+
		sta ,x
		leax 32,x
		decb
		bne <
		ldx #$042e
		leay 1,u

		ldb rpoffset
topdelay	equ *+1
		addb #$00

		; Wait for FS IRQ
		lda reg_pia0_pdrb	; clear IRQ
		sync			; wait for next IRQ

		; Delay topdelay+rpoffset lines
		bsr wait_blines	; 12 (+12 line)

		; Offset mode changes beyond end of visible line
		exg a,a		; +8 = 20
		exg a,a		; +8 = 28
		exg a,a		; +8 = 36
		exg a,a		; +8 = 44
		pshs		; +5 = 49 (+12 line)

		; Trigger VDG Row Preset
		ldd #$e800		; +3 = 52
		sta reg_pia1_pdrb	; +4 = 56		to graphics mode
		stb reg_pia1_pdrb	; +4 = 3 (+13 line)	to text mode

		; Timing
		exg a,a		; +8 = 11
		exg a,a		; +8 = 19
		exg a,a		; +8 = 27
		exg a,a		; +8 = 35
		exg a,a		; +8 = 43
		pshs		; +5 = 48 (+13 line)

		; ---

		; Display 14 lines of message.  This will update the cell
		; contents with the next character of the message just
		; before the VDG gets to it.

		lda #14		; +2 = 50 (+13 line)

		; 11 scanline delay plus housekeeping timed to exactly
		; equal 1 scanline means each loop lasts exactly one text
		; line (12 scanlines).

100		ldb #11		;; 2
		bsr wait_blines	;; +12 = 14

		; Update character for cell
		ldb ,y+		;; +6 = 20
		stb ,x		;; +4 = 24
		leax 32,x	;; +5 = 29

		; Timing
		exg a,a		;; +8 = 37
		exg a,a		;; +8 = 45
		pshs		;; +5 = 50
		nop		;; +2 = 52

		deca		;; +2 = 54
		bne 100B	;; +3 = 57

		; ---

		; Additional delay so that the prerender for the next
		; frame isn't noticeable on the bottom line of text
		ldb #11
		bsr wait_blines

		; Debug: wait for SHIFT to be pressed
	if DEBUG == 1
		ldd #$7f40
		sta reg_pia0_pdrb
		andb reg_pia0_pdra
kbdtmp		equ *+1
		cmpb #$00
		beq >
		stb kbdtmp
		bne >
	endif

		; Loop from 12..1 RP offsets
		dec rpoffset
		bne >
		lda #12
		sta rpoffset
		leau 1,u
!

		; Loop message
		lda 1,y		; end of message?
		beq restart
		bra mainloop

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Delay for B scanlines.  Caller accounts for BSR/JSR and RTS timing.
wait_blines
!		exg a,a		;; 8
		exg a,a		;; +8 = 16
		exg a,a		;; +8 = 24
		exg a,a		;; +8 = 32
		exg a,a		;; +8 = 40
		tfr a,a		;; +6 = 46
		tfr a,a		;; +6 = 52
		decb		;; +2 = 54
		bne <		;; +3 = 57
		rts		; 5: account for this before call!

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Gotta have a silly message...

message		fci /...............JUST A LITTLE SCROLLING DEMO... /
		fci /YES, THIS IS THE NORMAL 512 BYTE TEXT SCREEN.  DO /
		fci /YOU KNOW WHAT'S GOING ON?  NO PRIZES...  THIS /
		fci /FORM OF HARDWARE SCROLLING DOES NOT INVOLVE /
		fci /THE SAM AT ALL.     COOL?      SHOUT OUTS TO /
		fci /THE ORCHARDS...  BOSCO...  INVISIBLEMAN...  AND /
		fci /ALL THE BUS ROUTES..................../,0

rpoffset	fcb 12
