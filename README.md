# 512scroll

The key to this demo is that the font row is reset each time the VDG is
switched from graphics to text mode, regardless of which scanline it is
drawing.
