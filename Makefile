# 512scroll by Ciaran Anscomb 2021-2023
# For Dragon/Tandy CoCo 1/2 (4K compatible!).

# Tiny demo of manually invoking VDG row preset.

PACKAGE_NAME = 512scroll
PACKAGE_VERSION = 0.2

default: tape
.PHONY: default

tape: 512scroll.cas 512scroll.wav 512scroll.dsk
.PHONY: tape

####

ASM6809 = asm6809 -v
BIN2CAS = bin2cas.pl
CLEAN =
EXTRA_DIST =
XROAR = xroar -ui null -ao null -no-ratelimit


####

512scroll.bin: 512scroll.s
	$(ASM6809) -l $(@:.bin=.lis) -o $@ -C $<

CLEAN += 512scroll.bin 512scroll.lis

512scroll.cas 512scroll.wav: 512scroll.bin
	$(BIN2CAS) -r 44100 --cue --autorun --eof-data --fast -o $@ -n "DEMO" \
		-C --dzip 512scroll.bin

CLEAN += 512scroll.cas 512scroll.wav

####

# Boot block.  Same image is used for DragonDOS and RSDOS disks.

booty.bin: booty.s
	$(ASM6809) -l $(@:.bin=.lis) -o $@ -B $<

CLEAN += booty.lis booty.bin

####

512scroll-disk.bin: 512scroll.s
	$(ASM6809) -d DISK -l $(@:.bin=.lis) -o $@ -C $<

CLEAN += 512scroll-disk.bin 512scroll-disk.lis

binfiles.cas: 512scroll-disk.bin
	bin2cas.pl -o $@ \
		-C 512scroll-disk.bin

CLEAN += binfiles.cas

512scroll.dsk: binfiles.cas
512scroll.dsk: 512scroll-disk.bin
512scroll.dsk: 512scroll-dragon-disk.txt
512scroll.dsk: hybrid.dsk booty.bin

512scroll.dsk:
	cp hybrid.dsk $@
	decb copy -2b 512scroll-disk.bin -2b $@,512SCR.BIN
	$(XROAR) -cart dragondos -cart-rom dplus49b -disk-write-back -load-fd0 $@ -load-tape binfiles.cas -load-text 512scroll-dragon-disk.txt -timeout-motoroff 12
	dd if=booty.bin of=$@ bs=256 seek=2 conv=notrunc
	dd if=booty.bin of=$@ bs=256 seek=$(shell expr 34 \* 18) conv=notrunc

CLEAN += 512scroll.dsk 512scroll.dsk.bak

####

distdir = $(PACKAGE_NAME)-$(PACKAGE_VERSION)

dist:
	git archive --format=tar --prefix=$(distdir)/ HEAD > $(distdir).tar
	gzip -f9 $(distdir).tar
.PHONY: dist

clean:
	rm -f $(CLEAN)
.PHONY: clean
